package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Entry struct {
	time time.Time
	nt   uint64
	vt   uint64
}

type Entries []Entry

func (entry Entry) String() string {
	return fmt.Sprintf("%s;%d;%d\n", entry.time.Format("02.01.2006 15:04"), entry.nt, entry.vt)
}

func (s Entries) Len() int {
	return len(s)
}
func (s Entries) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (entries Entries) Less(i, j int) bool {
	return entries[i].time.Before(entries[j].time)
}

func (entries Entries) printStats() {
	var lastEntry Entry = entries[0]
	for i := 1; i < len(entries); i++ {
		var vt = entries[i].vt - lastEntry.vt
		var nt = entries[i].nt - lastEntry.nt
		var avvt float64 = float64(vt) / float64( entries[i].time.Unix() - lastEntry.time.Unix() ) * (24 * 3600)
		var avnt float64 = float64(nt) / float64( entries[i].time.Unix() - lastEntry.time.Unix() ) * (24 * 3600)
		fmt.Printf("datum: %s, vt: %d  nt: %d\n", entries[i].time.Format("02.01.2006 15:04"), entries[i].vt, entries[i].nt)
		if i > 0 {
			fmt.Printf("spotreba vt: %d, prumer na den: %2f, nt: %d, na den: %2f\n\n", entries[i].vt-lastEntry.vt, avvt, entries[i].nt-lastEntry.nt, avnt)
		}
		lastEntry = entries[i]
	}
	return
}

func getNewEntry() (Entry, error) {
	var eStr, eIo error
	b := bufio.NewReader(os.Stdin)
	var s string
	var entry Entry

	now := time.Now().Format("02.01.2006 15:04")
	fmt.Print("datum [", now, "]: ")
	s, eIo = b.ReadString('\n')
	if eIo == io.EOF {
		return entry, eIo
	}
	if s == "\n" {
		s = now
	}
	// 01/02 03:04:05PM '06 -0700
	// Mon Jan 02 15:04:05 -0700 2006
	entry.time, eStr = time.Parse("02.01.2006 15:04", strings.TrimSpace(s))
	if eStr != nil {
		return entry, eStr
	}
	fmt.Printf("vt: ")
	s, eStr = b.ReadString('\n')
	entry.vt, eStr = strconv.ParseUint(strings.TrimSpace(s), 0, 64)
	if eStr != nil {
		return entry, eStr
	}
	fmt.Printf("nt: ")
	s, eStr = b.ReadString('\n')
	entry.nt, eStr = strconv.ParseUint(strings.TrimSpace(s), 0, 64)
	if eStr != nil {
		return entry, eStr
	}
	return entry, nil
}

func readEntriesFromFile(filename string) (Entries, error) {
	entries := Entries{}
	f, e := os.Open(filename)
	if e != nil {
		return nil, e
	}
	s := bufio.NewScanner(f)
	for s.Scan() {
		eLine := s.Text()
		f := strings.Split(eLine, ";")
		var eStr error
		entry := Entry{}
		entry.time, eStr = time.Parse("02.01.2006 15:04", f[0])
		if eStr != nil {
			return nil, eStr
		}
		entry.vt, eStr = strconv.ParseUint(f[2], 0, 64)
		if eStr != nil {
			return nil, eStr
		}
		entry.nt, eStr = strconv.ParseUint(f[1], 0, 64)
		if eStr != nil {
			return nil, eStr
		}
		entries = append(entries, entry)
	}
	return entries, nil
}

func main() {
	var update bool = false
	var entries Entries
	entries, _ = readEntriesFromFile("elektrina.csv")

	fmt.Println("Zadavani ukonci C-d ...")
	for {
		entry, e := getNewEntry()
		if e != nil {
			if e == io.EOF {
				break
			}
			fmt.Println(e)
			continue
		}
		entries = append(entries, entry)
		update = true
	}

	sort.Sort(Entries(entries))

	if update {
		f, err := os.Create("elektrina.csv")
		if err != nil {
			panic(err)
		}
		defer f.Close()
		for _, entry := range entries {
			f.WriteString(entry.String())
		}
	}

	entries.printStats()

}
